#! /bin/sh
#
# Copyright (C) 2009 sonojacker / Sistema de Teatros de la Ciudad de México
# # This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# AWFULLY set hardcoded vars. Please do something.
set_hardcoded_vars()
{
  # Hardcoded filenames and dirs
  DIRDES="pdfs" # Set PDFs output directory
  DIRDAT="data" # Set PDFs output directory
  DIRTPL="templates" # Set PDFs output directory
  CSVFIL="$DIRDAT/habitante.csv" # Set 4-column CSV data file
  SVGFIL="$DIRTPL/habitante-template_cred.svg" # SVG template
  SVGFILFULL="$DIRTPL/habitante-template_full.svg" # SVG template

  # Hardcoded bins
  QRENCODE="/usr/bin/qrencode"
  INKSCAPE="/usr/bin/inkscape"

  # Hardcoded searches in SVG template to be replaced
  SEDFOL="00folio"  
  SEDNAM="Sara"
  SEDAPE="Cruz"
  SEDQRC="qrcode.png"

  # Set version
  HABI_VE="0.01pre-alpha"
}

set_vars()
{

  # Get this script filename and command
  HABI_SH=$(basename "$0")
  export HABI_SH
  the_command="$HABI_SH"

  # Create TEMP files
  TMPDIR="${TMPDIR-/tmp}"
  TEMPFILENAME=`mktemp 2>/dev/null || echo "$TMPDIR/hab-tmp$$.svg"`
  TEMPPNG=`mktemp 2>/dev/null || echo "$TMPDIR/hab-tmp$$.png"`

  # TODO Please create data file and template tester

  mkdir $DIRDES 2> /dev/null && echo "Had to create export directory at: $DIRDES"

}

csv_worker()
{
  while IFS=, read -r folio nombre apellido correo loc
  do
    folio=$(printf "%06d" $folio)

    echo "Procesando: $folio|$nombre $apellido"

    $QRENCODE -m 2 -s 6 -t png -o $TEMPPNG "$folio|$nombre $apellido"

    sed -e "s/$SEDNAM/$nombre/;\
      s/$SEDAPE/$apellido/;\
      s/$SEDFOL/$folio/;\
      s/$SEDQRC/$(echo $TEMPPNG|sed 's/\//\\\//g')/" $SVGFIL > $TEMPFILENAME &&\
      $INKSCAPE -z $TEMPFILENAME -A $DIRDES/$folio-cred.pdf 2>/dev/null

    sed -e "s/$SEDNAM/$nombre/;\
      s/$SEDAPE/$apellido/;\
      s/$SEDFOL/$folio/;\
      s/$SEDQRC/$(echo $TEMPPNG|sed 's/\//\\\//g')/" $SVGFILFULL > $TEMPFILENAME &&\
      $INKSCAPE -z $TEMPFILENAME -A $DIRDES/$folio.pdf 2>/dev/null

  done < $CSVFIL
}

clean() 
{
  rm -rf $TEMPFILENAME $TEMPPNGNAME 
}

openingMessage()
{
    cat <<-EndOptionsHelp
      $HABI_SH $HABI_VE (C) LGPL sonojacker.
        Usage: $HABI_SH
        (Please modify hardcoded values)
	EndOptionsHelp
}

set_hardcoded_vars
set_vars
openingMessage
csv_worker
clean
